#!/usr/bin/perl

=head1 NAME

dummy script doing nothing

=cut

use strict;

use FileHandle;

binmode( STDOUT, ':utf8' ); autoflush STDOUT 1;
binmode( STDERR, ':utf8' ); autoflush STDERR 1;
binmode( STDIN,  ':utf8' );

use Data::Dumper;
$Data::Dumper::Indent= 1;

use lib 'lib';

use OeFOS::Data;
use Phaidra::Classification;
use Phaidra::Classification::DB;

my $fnm_oefos_de= 'OEFOS2012_DE_CTI_20171007_030438.txt';
my $fnm_oefos_en= 'OEFOS2012_EN_CTI_20171007_030427.txt';
my $oefos_version= '20171007';

my %phaidradb_oefos_files=
(
  'classification_name' => 'oefos',
  'taxon' => 'taxon.16.tsv',
  'voc' => 'voc.44.tsv',
  'taxon_vocentry' => 'taxon_vocentry.16.tsv',
);

my $x_flag= 0;

=begin comment

TODO:
* check the current max() ID values in the database and update the script here if necessary
* if we would talk directly to the database, we could query these values

select max(TID) from taxon;
select max(VEID) from vocabulary_entry;
select max(VEID), max(TID), max(TVID) from taxon_vocentry;

=end comment
=cut

my $pcdb= new Phaidra::Classification::DB (max_TID => 1073506, max_VEID => 1562640, max_TVID => 1562583, CID => 16, VID => 44);

my @PARS;
my $arg;
while (defined ($arg= shift (@ARGV)))
{
  if ($arg eq '--') { push (@PARS, @ARGV); @ARGV= (); }
  elsif ($arg =~ /^--(.+)/)
  {
    my ($opt, $val)= split ('=', $1, 2);

    if ($opt eq 'help') { usage(); }
    else { usage(); }
  }
  elsif ($arg =~ /^-(.+)/)
  {
    foreach my $opt (split ('', $1))
    {
         if ($opt eq 'h') { usage(); exit (0); }
      elsif ($opt eq 'x') { $x_flag= 1; }
      else { usage(); }
    }
  }
  else
  {
    push (@PARS, $arg);
  }
}

print join (' ', __FILE__, __LINE__, 'caller=['. caller() . ']'), "\n";

my $oefos= process_oefos_lists();
my $c16= get_phaidra_classificaton ('phaidra/prod', \%phaidradb_oefos_files);

check_classification ($c16, $oefos, $phaidradb_oefos_files{classification_name});

exit (0);

sub usage
{
  print <<EOX;
usage: $0 [-opts] pars

template ...

options:
-h  ... help
-x  ... set x flag
--  ... remaining args are parameters
EOX

  exit (0);
}

sub process_oefos_lists
{
  my $oefos= new OeFOS::Data;

  $oefos->read_classification_csv ($fnm_oefos_de, 'de', $oefos_version);
  $oefos->read_classification_csv ($fnm_oefos_en, 'en', $oefos_version);

  # print __LINE__, " oefos: ", main::Dumper ($oefos);

  $oefos;
}

sub get_phaidra_classificaton
{
  my $base_dir= shift;
  my $filenames= shift;

  my $classification= new Phaidra::Classification (base_dir => $base_dir);
  $classification->read_phaidradb_data ($filenames);

  # print __LINE__, " classification: ", main::Dumper ($classification);
  # print __LINE__, " voc: ", main::Dumper ($classification->{voc});
  # print __LINE__, " oefos: ", main::Dumper ($classification->{$filenames->{classification_name}});

  $classification;
}

sub check_classification
{
  my $classification= shift;
  my $upstream= shift;
  my $cname= shift;

  my $codes= $upstream->{codes};
  my ($tids, $voc)= map { $classification->{$_} } qw(tid voc);
  my $map= $classification->{$cname};

  foreach my $code (sort keys %$codes)
  {
    my $x= $codes->{$code};
    my $tid= $map->{$code};

    my $add= oefos_to_pcdb ($x);

    if (defined ($tid))
    {
      my $taxon= $tids->{$tid};
      print "code=[$code] tid=[$tid]\n";

      my $TID_parent;
      if (exists ($add->{upstream_parent}))
      {
        my $p= $add->{upstream_parent};
        $TID_parent= $map->{$p};
      }

      compare_oefos_and_pcdb ($add, $taxon, $TID_parent);
    }
    else
    {
      print "ATTN: code=[$code] taxon missing! x: ", Dumper ($x);


      if (exists ($add->{upstream_parent}))
      {
        my $p= $add->{upstream_parent};
        my $TID_parent= $map->{$p};

        if (defined ($TID_parent))
        {
          $add->{TID_parent}= $TID_parent;
        }
        else
        {
          print "ATTN: upstream_TID not defined for upstream_parent=[$p]\n";
        }
      }

      my $new_tid= $pcdb->add ($add);
      $map->{$code}= $new_tid;
    }

  }
}

sub compare_oefos_and_pcdb
{
  my $oe= shift;
  my $pc= shift;
  my $real_tid_parent= shift;

  print "oefos: ", Dumper ($oe), "taxon: ", Dumper ($pc);
  print "real_tid_parent=[$real_tid_parent]\n";

  if ((my $tp= $pc->{TID_parent}) ne $real_tid_parent)
  {
    print "ATTN: TID_parent wrong! current=[$tp] real=[$real_tid_parent]\n";
    my $upd1= "UPDATE taxon SET TID_parent=$real_tid_parent WHERE TID=$pc->{TID}";
    print ">>> ", $upd1, ";\n";
  }

  my @veids= sort keys %{$pc->{veid}};
  if (@veids == 0)
  {
    print "ATTN: no VEID present in current record: ", Dumper ($pc);
    return undef;
  }

  my $veid= shift (@veids);
  if (@veids > 0)
  {
    print "ATTN: more than one VEID present in current record, using VEID=[$veid]: ", Dumper ($pc);
  }
  my $pc_v= $pc->{veid}->{$veid};
  my $pc_voc= $pc_v->{voc};

  my @isocodes= sort keys %{$oe->{isocode}};
  if (@isocodes == 0)
  {
    print "ATTN: no isocode present in OEFOS record: ", Dumper ($oe);
    return undef;
  }

  if (@isocodes != 2)
  {
    print "ATTN: unexpected number of isocodes present in OEFOS record: ", Dumper ($oe);
  }

  foreach my $isocode (@isocodes)
  {
    my $entry= $oe->{isocode}->{$isocode};
    my $code= $oe->{upstream_identifier};

    if (!exists ($pc_voc->{$isocode}))
    {
      print "ATTN/TODO: isocode=[$isocode] missing from current record: ", Dumper ($pc);
    }
    elsif ($pc_voc->{$isocode}->{entry} ne $entry)
    {
      print "ATTN: entry for isocode=[$isocode] not ok entry=[$entry]: pc: ", Dumper ($pc);
      print "ATTN: entry for isocode=[$isocode] not ok entry=[$entry]: pc_voc: ", Dumper ($pc_voc);
      print "ATTN: entry for isocode=[$isocode] not ok entry=[$entry]: oe: ", Dumper ($oe);
      my $upd2= "UPDATE vocabulary_entry SET entry='$entry' WHERE VEID=$veid AND isocode='$isocode'";
      print ">>> ", $upd2, ";\n";
    }
    else
    {
      print "NOTE: entry entry ok isocode=[$isocode] entry=[$entry]\n";
    }
  }
}

sub oefos_to_pcdb
{
  my $oe= shift;

  my $v= $oe->{versions}->{$oefos_version};
  my $de= $v->{de};
  my $en= $v->{en};

  my $res=
  {
    upstream_identifier => $oe->{code},
    description => $de->{title},
    isocode => { de => $de->{title}, en => $en->{title} },
  };

  if ($de->{level} > 1)
  {
    $res->{upstream_parent}= $de->{parent};
  }

  $res;
}

__END__

=head1 AUTHOR

Firstname Lastname <address@example.org>

