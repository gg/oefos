#!/usr/bin/perl
#
# $Id: Module.pm,v 1.6 2017/10/09 07:39:48 gonter Exp $

package Phaidra::Classification::DB;

use strict;

__PACKAGE__->main() unless caller();

sub main
{
  print join (' ', __FILE__, __LINE__, 'main: caller=['. caller(). ']'), "\n";
}

sub new
{
  my $class= shift;

  my $obj= {};
  bless $obj, $class;

  $obj->set (@_);

  $obj;
}

sub set
{
  my $obj= shift;
  my %par= @_;

  my %res;
  foreach my $par (keys %par)
  {
    $res{$par}= $obj->{$par};
    $obj->{$par}= $par{$par};
  }

  (wantarray) ? %res : \%res;
}

sub get_array
{
  my $obj= shift;
  my @par= @_;

  my @res;
  foreach my $par (@par)
  {
    push (@res, $obj->{$par});
  }

  (wantarray) ? @res : \@res;
}

sub get_hash
{
  my $obj= shift;
  my @par= @_;

  my %res;
  foreach my $par (@par)
  {
    $res{$par}= $obj->{$par};
  }

  (wantarray) ? %res : \%res;
}

*get= *get_array;

sub add
{
  my $self= shift;
  my $x= shift;

  print __LINE__, " add: x: ", main::Dumper ($x);

  my $tid= ++$self->{max_TID};
  my @vars= qw(TID CID description upstream_identifier);
  my @vals= ($tid, $self->{CID}, $x->{description}, $x->{upstream_identifier});

  if (exists ($x->{TID_parent}))
  {
    push (@vars, 'TID_parent');
    push (@vals, $x->{TID_parent});
  }

  my $insert_taxon= insert ('taxon', \@vars, \@vals);
  print ">>> ", $insert_taxon, ";\n";

  my $veid= ++$self->{max_VEID};
  foreach my $isocode (sort keys %{$x->{isocode}})
  {
    my @vars_voc= qw(VEID isocode entry VID);
    my @vals_voc= ($veid, $isocode, $x->{isocode}->{$isocode}, $self->{VID});

    my $insert_voc= insert ('vocabulary_entry', \@vars_voc, \@vals_voc);
    print ">>> ", $insert_voc, ";\n";
  }

  my $tvid= ++$self->{max_TVID};
  my @vars_tv= qw(TVID TID VEID preferred term_id);
  my @vals_tv= ($tvid, $tid, $veid, 1, $x->{upstream_identifier});

  my $insert_tv= insert ('taxon_vocentry', \@vars_tv, \@vals_tv);
  print ">>> ", $insert_tv, ";\n";

  $tid;
}

sub insert
{
  my $table= shift;
  my $vars= shift;
  my $vals= shift;
  
  my $l= "INSERT INTO $table (". join (', ', @$vars). ") VALUES ('" . join ("', '", @$vals) . "')";
}


1;

__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 DATABASE STRUCTURE

=head2 taxon
 +---------------------+--------------+------+-----+---------+----------------+
 | Field               | Type         | Null | Key | Default | Extra          |
 +---------------------+--------------+------+-----+---------+----------------+
 | TID                 | int(11)      | NO   | PRI | NULL    | auto_increment |
 | TID_parent          | int(11)      | YES  | MUL | NULL    |                |
 | CID                 | int(11)      | NO   |     | NULL    |                |
 | description         | varchar(255) | YES  |     | NULL    |                |
 | upstream_identifier | varchar(255) | YES  | MUL | NULL    |                |
 +---------------------+--------------+------+-----+---------+----------------+

=head2 vocabulary_entry
 +---------+---------------+------+-----+---------+-------+
 | Field   | Type          | Null | Key | Default | Extra |
 +---------+---------------+------+-----+---------+-------+
 | VEID    | int(11)       | NO   | PRI | NULL    |       |
 | isocode | varchar(10)   | NO   | PRI | NULL    |       |
 | entry   | varchar(2048) | YES  | MUL | NULL    |       |
 | VID     | int(11)       | NO   |     | NULL    |       |
 +---------+---------------+------+-----+---------+-------+

=head2 taxon_vocentry
 +-----------+-------------+------+-----+---------+----------------+
 | Field     | Type        | Null | Key | Default | Extra          |
 +-----------+-------------+------+-----+---------+----------------+
 | TVID      | int(11)     | NO   | PRI | NULL    | auto_increment |
 | TID       | int(11)     | NO   | MUL | NULL    |                |
 | VEID      | int(11)     | NO   | MUL | NULL    |                |
 | preferred | tinyint(1)  | NO   |     | 1       |                |
 | term_id   | varchar(64) | YES  |     | NULL    |                |
 +-----------+-------------+------+-----+---------+----------------+

=head1 BUGS

=head1 REFERENCES

=head1 AUTHOR

