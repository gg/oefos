#!/usr/bin/perl
#
# $Id: Module.pm,v 1.6 2017/10/09 07:39:48 gonter Exp $

package Phaidra::Classification;

use strict;

use Util::Simple_CSV;
use Data::Dumper;

__PACKAGE__->main() unless caller();

sub main
{
  print join (' ', __FILE__, __LINE__, 'main: caller=['. caller(). ']'), "\n";
}

sub new
{
  my $class= shift;

  my $obj=
  {
    tid => {},
    voc => {},
  };
  bless $obj, $class;

  $obj->set (@_);

  $obj;
}

sub set
{
  my $obj= shift;
  my %par= @_;

  my %res;
  foreach my $par (keys %par)
  {
    $res{$par}= $obj->{$par};
    $obj->{$par}= $par{$par};
  }

  (wantarray) ? %res : \%res;
}

sub get_array
{
  my $obj= shift;
  my @par= @_;

  my @res;
  foreach my $par (@par)
  {
    push (@res, $obj->{$par});
  }

  (wantarray) ? @res : \@res;
}

sub get_hash
{
  my $obj= shift;
  my @par= @_;

  my %res;
  foreach my $par (@par)
  {
    $res{$par}= $obj->{$par};
  }

  (wantarray) ? %res : \%res;
}

*get= *get_array;

sub find_taxon
{
  my $self= shift;
  my $tid= shift;

  my $x= $self->{tids}->{$tid};
     $x= $self->{tids}->{$tid}= { };
}

sub read_phaidradb_data
{
  my $self= shift;
  my $filenames= shift;

  my $taxon_data= get_data ($self->{base_dir}, $filenames->{taxon});
  $self->{$filenames->{classification_name}}= $self->merge_taxons ($taxon_data);

  my $voc_data= get_data ($self->{base_dir}, $filenames->{voc});
  $self->merge_vocabulary ($voc_data);

  my $tv_data= get_data ($self->{base_dir}, $filenames->{taxon_vocentry});
  $self->link_taxons_and_vocabulary ($tv_data);

}

sub link_taxons_and_vocabulary
{
  my $self= shift;
  my $tve_list= shift;

  my $tids= $self->{tid};
  my $voc= $self->{voc};

  foreach my $tve (@$tve_list)
  {
    my ($tid, $veid)= map { $tve->{$_} } qw(TID VEID);
    $tids->{$tid}->{veid}->{$veid}= $tve;
    $voc->{$veid}->{tid}->{$tid}=   $tve;

    $tve->{taxon}= $tids->{$tid};
    $tve->{voc}=   $voc->{$veid};
  }
}

sub merge_taxons
{
  my $self= shift;
  my $taxon_list= shift;

  my $tids= $self->{tid};

  my %upstream_ids;
  foreach my $taxon (@$taxon_list)
  {
# print __LINE__, " taxon: ", Dumper ($taxon);
    my $tid= $taxon->{TID};
    if (exists ($tids->{$tid}))
    {
      print "ATTN: taxon already exists: TID=[$tid] previous: ", Dumper ($tids->{$tid}), "new: ", Dumper ($taxon);
      next;
    }

    $tids->{$tid}= $taxon;
    $upstream_ids{$taxon->{upstream_identifier}}= $tid;
  }

# print __LINE__, " upstream_ids: ", Dumper (\%upstream_ids);
  (wantarray) ? %upstream_ids : \%upstream_ids;
}

sub merge_vocabulary
{
  my $self= shift;
  my $voc_list= shift;

  my $voc= $self->{voc};

  foreach my $voc_item (@$voc_list)
  {
    my ($veid, $lang)= map { $voc_item->{$_} } qw(VEID isocode);

    if (exists ($voc->{$veid}->{$lang}))
    {
      print "ATTN: voc entry already exists for lang=[$lang], veid=[$veid]; previous: ", Dumper ($voc->{$veid}->{$lang}), "new: ", Dumper ($voc_item);
      next;
    }
    
    $voc->{$veid}->{$lang}= $voc_item;
  }
}

sub get_data
{
  my $base_dir= shift;
  my $fnm= shift;

  my $fnm= join ('/', $base_dir, $fnm);

  my $csv= new Util::Simple_CSV (load => $fnm, separator => "\t", UTF8 => 1);
  my $data= $csv->{data};
  # print __LINE__, " data: ", Dumper ($data);

  $data;
}

1;

__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 BUGS

=head1 REFERENCES

=head1 AUTHOR

