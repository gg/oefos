#!/usr/bin/perl
#
# $Id: Module.pm,v 1.6 2017/10/09 07:39:48 gonter Exp $

package OeFOS::Data;

use strict;

use Data::Dumper;
use Util::Simple_CSV;

__PACKAGE__->main() unless caller();

sub main
{
  print join (' ', __FILE__, __LINE__, 'main: caller=['. caller(). ']'), "\n";
}

sub new
{
  my $class= shift;

  my $obj= {};
  bless $obj, $class;

  $obj->set (@_);

  $obj;
}

sub set
{
  my $obj= shift;
  my %par= @_;

  my %res;
  foreach my $par (keys %par)
  {
    $res{$par}= $obj->{$par};
    $obj->{$par}= $par{$par};
  }

  (wantarray) ? %res : \%res;
}

sub get_array
{
  my $obj= shift;
  my @par= @_;

  my @res;
  foreach my $par (@par)
  {
    push (@res, $obj->{$par});
  }

  (wantarray) ? @res : \@res;
}

sub get_hash
{
  my $obj= shift;
  my @par= @_;

  my %res;
  foreach my $par (@par)
  {
    $res{$par}= $obj->{$par};
  }

  (wantarray) ? %res : \%res;
}

*get= *get_array;

sub find
{
  my $self= shift;
  my $code= shift;

  my $rec= $self->{codes}->{$code};
     $rec= $self->{codes}->{$code}= { code => $code } unless (defined ($rec));

  $rec;
}

sub read_classification_csv
{
  my $self= shift;
  my $fnm= shift;
  my $lang= shift;
  my $version= shift;

  my $csv= new Util::Simple_CSV (load => $fnm, strip_quotes => 1);
  my $data= $csv->{data};

  my @level;
  foreach my $row (@$data)
  {
    # print __LINE__, " row: ", Dumper ($row);

# "Ebene";"EDV-Code";"Code";"Titel";"Kurztitel"

    my $rec= $self->find (my $code= $row->{Code});

    my $version_info= 
    {
      level => my $level= $row->{Ebene},
      title => $row->{Titel},
    };

    $version_info->{short_title}= $row->{Kurztitel} if (defined ($row->{Kurztitel}) && $row->{Kurztitel} ne '');
    $version_info->{parent}= $level[$level-1] if ($level > 1);
    $level[$level]= $code if ($level < 4);

    $rec->{versions}->{$version}->{$lang}= $version_info;

    # print __LINE__, " rec: ", Dumper ($rec);
  }
}


1;

__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 BUGS

=head1 REFERENCES

=head1 AUTHOR

